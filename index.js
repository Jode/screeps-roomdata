const request	= require("request-promise-native");
const fs		= require("fs");

const ROOM_DATA_ENDPOINT = (shard) => `https://www.leagueofautomatednations.com/map/${shard}/rooms.js`;
const OUTPUT_ENDPOINT = "https://screeps.com/api/user/memory-segment";

async function updateOwnershipData(config) {
	let roomDataIn = await request({ url: ROOM_DATA_ENDPOINT(config.shard), json: true });

	let output = [];
	for (let [roomName, data] of Object.entries(roomDataIn)) {
		if (data.level !== 0) {
			output.push(`${roomName} ${data.owner}`);
		}
	}

	output = output.join(";");

	let response = await request({
		url: OUTPUT_ENDPOINT,
		method: "POST",
		json: true,
		headers: { "X-Token": config.token },
		body: { shard: config.shard, segment: config.segment, data: output }
	});

	if (response.ok !== 1) {
		throw `Failed to upload room data; Screeps API returned: \n{JSON.stringify(response)}`;
	} else {
		console.log("Updated room data successfully!");
	}
}

fs.readFile("config.json", (err, data) => {
	if (err) { throw err; }

	data = JSON.parse(data);
	updateOwnershipData(data);
});