`screeps-roomdata` pulls data on room ownership from the [LOAN API](https://www.leagueofautomatednations.com/api) and uploads that it to a memory segment in a more compressed format.
It is designed to be used with cron or some other scheduling mechanism.

Example config.json:
```json
{
"shard": "shard1",
"segment": 10,
"token": "1dfb9249-9c66-45b0-a6d8-4f2c3601b6cf"
}
```

Example data format:
`W12N48 Jode;W17N45 TuN9aN0;W1N44 ags131`

Example usage:
```js
function updateRoomOwnership() {
	if (!global.RoomOwners || Game.time % 1000 === 0 || Game.time % 1000 === 1) {
		if (RawMemory.segments[10]) {
			global.RoomOwners = {};

			let rawData = RawMemory.segments[10];
			for (let entry of rawData.split(';')) {
				let [room, owner] = entry.split(' ');
				RoomOwners[room] = owner;
			}
		} else {
			RawMemory.setActiveSegments([10]);
		}
	}
}
```